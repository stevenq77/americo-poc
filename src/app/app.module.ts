import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialComponent } from './app.component.material';
import { routing } from './app.component.routing';
import { AppMenuComponent } from './components/main-menu/app.menu.component';
import { NewQuoteComponent } from './components/new-quote/app.newquote.component';

@NgModule({
  declarations: [
    AppComponent,
    AppMenuComponent,
    NewQuoteComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    routing,
    MaterialComponent
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})

export class AppModule { }
