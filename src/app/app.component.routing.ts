import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Application Main Menu
import { AppMenuComponent } from './components/main-menu/app.menu.component';

// Application New Quote
import { NewQuoteComponent } from './components/new-quote/app.newquote.component';

// Main application routing
const navRouting: Routes = [
    { path: '', component: AppMenuComponent },
    { path: 'quote', component: NewQuoteComponent}
];

// Export Nav Routing
export const routing: ModuleWithProviders = RouterModule.forRoot(navRouting);
